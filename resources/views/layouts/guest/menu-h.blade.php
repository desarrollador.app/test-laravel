<div 
  class="flex  ml-10 space-x-4 h-16"
  x-data="{ open: true }"
>
    <button 
      class="text-gray-600 py-4 px-4 block hover:text-blue-500 focus:outline-none font-medium" 
      x-bind:class="open ? 'text-blue-500 border-b-2 border-blue-500' : ''"
    >Portada</button>
    <button 
      class="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">Tab 2</button>
    <button 
      class="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">Tab 3</button>
    <button 
      class="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">Tab 4</button>

</div>