<div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
  <div class="flex items-center justify-between h-16">
    <div class="flex items-center">
      <div class="flex-shrink-0">
        <img class="h-8 w-8" src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg" alt="Workflow">
      </div>
      <div class="hidden md:block ">
        @include('layouts.guest.menu-h')
      </div>
    </div>
    
    <div class="hidden md:block">
      <div class="ml-4 flex items-center md:ml-6">
        <!-- Botón login -->
        <span class="sm:ml-3">
          <a 
           class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
           href="/login">
            <!-- Heroicon name: solid/check -->
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 laptop:hidden" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
            </svg>
            Iniciar sesión
          </a>
        </span>
      </div>
    </div>

    <div class="-mr-2 flex md:hidden">
      <!-- Mobile menu button-->
      <button        
        type="button"
        x-bind:class="openMenuMovil ? 'text-gray-400 bg-gray-100' : 'bg-blue-500 text-white'"
        class="inline-flex items-center justify-center p-2 rounded-md hover:bg-gray-200 hover:text-gray-400" 
        aria-controls="mobile-menu" 
        :aria-expanded="(openMenuMovil || isActive) ? 'true' : 'false'"        
        @click="openMenuMovil = !openMenuMovil">
        <span class="sr-only">Open main menu</span>
        <!--Heroicon name: outline/menu -->
        <svg :class="{'block': openMenuMovil, 'hidden': !openMenuMovil}" class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
        </svg>
        <!-- Heroicon name: outline/x -->
        <svg :class="{'block': !openMenuMovil, 'hidden': openMenuMovil}" class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
        </svg>
      </button>
    </div>
  </div>
</div>

<!-- Mobile menu, show/hide based on menu state. -->
<div class="md:hidden" id="mobile-menu"
x-show="! openMenuMovil" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" >
  
  @include('layouts.guest.menu-movil')

  <div class="pt-4 pb-3 border-t border-gray-300">
    <div class="mt-3 px-2 space-y-1">
      <!-- Heroicon name: solid/check -->      
      <a href="/login" class=" inline-flex items-center px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 laptop:hidden" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
        </svg>Inciar sesión</a>
    </div>
  </div>
</div>
