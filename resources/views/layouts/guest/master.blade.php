<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('titlesite')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">


        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
      <div x-init="console.log('Count is ' + $store.site.title)"></div>    
      <div>
        <nav class="bg-gray-50"  x-data="{ isActive: true, openMenuMovil: true }">
          @include('layouts.guest.header')          
        </nav>

        {{-- <header class="bg-white shadow">
          <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h1 class="text-3xl font-bold text-gray-900">
              Dashboard
            </h1>
          </div>
        </header> --}}

        <main>
          <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
            <!-- Replace with your content -->
            <div class="px-4 py-6 sm:px-0">
              <div class="border-4 border-dashed border-gray-200 rounded-lg h-96"></div>
            </div>
            <!-- /End replace -->
          </div>
        </main>
        <footer>
          @include('layouts.guest.footer')
        </footer>
      </div>
      <script src="{{ mix('js/app.js') }}" defer></script>
      <script>
        document.addEventListener('alpine:init', () => {
            /* State */
            Alpine.data('dropdown', () => ({
                open: false,
    
                toggle() {
                    this.open = ! this.open
                }
            }))

            /* Store */
            Alpine.store('site', {
              title: 'HOLA MUNDO',            
              notify(message) { 
                this.items.push(message)
              }
            })
        })
      </script>
      
    </body>
</html>
