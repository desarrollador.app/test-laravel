const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/views/layouts/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
        },
        borderWidth: {
            DEFAULT: '1px',
            '0': '0',
            '2': '2px',
            '3': '3px',
            '4': '4px',
            '6': '6px',
            '8': '8px',
          }
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],

    variants: {
        extend: {
          backgroundColor: ['hover', 'focus','active'],
          borderColor: ['hover', 'focus','active'],
          borderWidth: ['hover', 'focus','active'],
          visibility: ['responsive'],
        }
      },
};
